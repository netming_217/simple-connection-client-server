import socket			

s = socket.socket()		
print ("Socket successfully created")

port = 12345			
s.bind(('', port))		
print ("socket binded to %s" %(port))

s.listen(5)	
print ("socket is listening")	

c, addr = s.accept()
print ('Got connection from', addr )

ns = 1
while ns < 100:

    c.send(str(ns).encode())
    nc = c.recv(1024).decode()
    ns = int(nc)
    print (nc)

c.close()
